/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridadws.service;

import cr.ac.una.gestorseguridadws.model.Sistema;
import cr.ac.una.gestorseguridadws.model.SistemaDto;
import cr.ac.una.gestorseguridadws.util.CodigoRespuesta;
import cr.ac.una.gestorseguridadws.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Usuario
 */
@Stateless
@LocalBean
public class SistemaService {
    
    private static final Logger LOG = Logger.getLogger(SistemaService.class.getName());

    @PersistenceContext(unitName = "GestorSeguridadWsPU")
    private EntityManager em;
    
    public Respuesta getSistema(Long id) {
        try {
            Query qrySistema = em.createNamedQuery("Sistema.findBySisId", Sistema.class);
            qrySistema.setParameter("id", id);
            
            Sistema sistema = (Sistema) qrySistema.getSingleResult();
            SistemaDto sistemaDto = new SistemaDto(sistema);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Sistema", sistemaDto);
            
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un sistema con el código ingresado.", "getSistema NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el sistema.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el sistema.", "getSistema NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el sistema.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el sistema.", "getSistema " + ex.getMessage());
        }
    }
    
    public Respuesta getSistemas() {
        try {
            Query qrySistemas = em.createNamedQuery("Sistema.findAll", Sistema.class);
            List<Sistema> sistemas = qrySistemas.getResultList();
            
            List<Sistema> filtro = sistemas.stream().filter(s->s.getEstado().equals("A")).
                    collect(Collectors.toList());
            
            List<SistemaDto> sistemaDtos = new ArrayList<>();
            for (Sistema sistema : filtro) {
                sistemaDtos.add(new SistemaDto(sistema));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Sistemas", sistemaDtos);
            
        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Error consultando los sistemas.", "getSistemas NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar los sistemas.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar los sistemas.", "getSistemas " + ex.getMessage());
        }
    }
    
    public Respuesta guardarSistema(SistemaDto sistemaDto) {
        try {
            Sistema sistema;
            if (sistemaDto.getSisId() != null && sistemaDto.getSisId() > 0) {
                sistema = em.find(Sistema.class, sistemaDto.getSisId());
                if (sistema == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el sistema a modificar.", "guardarSistema NoResultException");
                }
                sistema.actualizarSistema(sistemaDto);
                sistema = em.merge(sistema);
            } else {
                sistema = new Sistema(sistemaDto);
                em.persist(sistema);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Sistema", new SistemaDto(sistema));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el sistema.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el sistema", "guardarSistema " + ex.getMessage());
        }
    }
    
    public Respuesta eliminarSistema(Long id) {
        try {
            Sistema sistema;
            if (id != null && id > 0) {
                sistema = em.find(Sistema.class, id);
                if (sistema == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el sistema a eliminar.", "eliminarSistema NoResultException");
                }
                em.remove(sistema);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el sistema a eliminar.", "eliminarSistema NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el sistema porque tiene relaciones con otros registros.", "eliminarSistema " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar el sistema.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el sistema.", "eliminarSistema " + ex.getMessage());
        }
    }
}
