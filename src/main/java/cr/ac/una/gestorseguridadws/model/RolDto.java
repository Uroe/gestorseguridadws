/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridadws.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author JosueNG
 */
public class RolDto {

    private Long rolId;
    private String rolNombre;
    private String rolEstado;
    private List<UsuarioDto> usuarios;
    private List<UsuarioDto> usuariosEliminados;
    private SistemaDto sistema;

    public RolDto() {
        usuarios = new ArrayList<>();
        usuariosEliminados = new ArrayList<>();
        sistema = new SistemaDto();
    }

    public RolDto(Rol rol) {
        this();
        this.rolId = rol.getId();
        this.rolNombre = rol.getNombre();
        this.rolEstado = rol.getEstado();
    }

    public Long getRolId() {
        return rolId;
    }

    public void setRolId(Long rolId) {
        this.rolId = rolId;
    }

    public String getRolNombre() {
        return rolNombre;
    }

    public void setRolNombre(String rolNombre) {
        this.rolNombre = rolNombre;
    }

    public String getRolEstado() {
        return rolEstado;
    }

    public void setRolEstado(String rolEstado) {
        this.rolEstado = rolEstado;
    }

    public List<UsuarioDto> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<UsuarioDto> usuarios) {
        this.usuarios = usuarios;
    }

    public SistemaDto getSistema() {
        return sistema;
    }

    public void setSistema(SistemaDto sistema) {
        this.sistema = sistema;
    }

    public List<UsuarioDto> getUsuariosEliminados() {
        return usuariosEliminados;
    }

    public void setUsuariosEliminados(List<UsuarioDto> usuariosEliminados) {
        this.usuariosEliminados = usuariosEliminados;
    }

    @Override
    public String toString() {
        return "RolDto{" + "id=" + rolId + ", nombre=" + rolNombre + ", estado=" + rolEstado + '}';
    }

}
