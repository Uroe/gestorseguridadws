package cr.ac.una.gestorseguridadws.service;

import cr.ac.una.gestorseguridadws.model.Rol;
import cr.ac.una.gestorseguridadws.model.RolDto;
import cr.ac.una.gestorseguridadws.model.Sistema;
import cr.ac.una.gestorseguridadws.model.SistemaDto;
import cr.ac.una.gestorseguridadws.model.Usuario;
import cr.ac.una.gestorseguridadws.model.UsuarioDto;
import cr.ac.una.gestorseguridadws.util.CodigoRespuesta;
import cr.ac.una.gestorseguridadws.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class RolService {

    private static final Logger LOG = Logger.getLogger(RolService.class.getName());

    @PersistenceContext(unitName = "GestorSeguridadWsPU")
    private EntityManager em;

    public Respuesta getRol(Long id) {
        try {
            Query qryRol = em.createNamedQuery("Rol.findByRolId", Rol.class);
            qryRol.setParameter("id", id);

            Rol rol = (Rol) qryRol.getSingleResult();
            RolDto rolDto = new RolDto(rol);
            rolDto.setSistema(new SistemaDto(rol.getSistema()));

            for (Usuario usuario : rol.getUsuarios()) {
                rolDto.getUsuarios().add(new UsuarioDto(usuario));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Rol", rolDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un rol con el código ingresado.", "getRol NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el rol.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el rol.", "getRol NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el rol.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el rol.", "getRol " + ex.getMessage());
        }
    }

    public Respuesta getRoles(Long id) {
        try {
            Query qryRol = em.createNamedQuery("Rol.findAll", Rol.class);

            List<Rol> roles = qryRol.getResultList();
            List<Rol> rolesxSis = new ArrayList<>();
            Sistema sistema;

            for (Rol rol : roles) {
                sistema = rol.getSistema();
                if (Objects.equals(sistema.getId(), id)) {
                    rolesxSis.add(rol);
                }
            }

            List<RolDto> rolDtos = new ArrayList<>();
            for (Rol rol : rolesxSis) {
                rolDtos.add(new RolDto(rol));
            }
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Roles", rolDtos);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existen roles con los criterios ingresados.", "getRoles NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el rol.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el rol.", "getRoles " + ex.getMessage());
        }
    }

    public Respuesta guardarRol(RolDto rolDto) {
        try {
            Sistema sistema = em.find(Sistema.class, rolDto.getSistema().getSisId());
            if (sistema == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró un sistema relacionado a este de rol", "guardarRol NoResultException");
            } else {
                Rol rol;
                if (rolDto.getRolId() != null && rolDto.getRolId() > 0) {
                    rol = em.find(Rol.class, rolDto.getRolId());
                    if (rol == null) {
                        return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el rol a modificar.", "guardarRol NoResultException");
                    }
                    rol.actualizarRol(rolDto);
                    for (UsuarioDto usu : rolDto.getUsuariosEliminados()) {
                        rol.getUsuarios().remove(new Usuario(usu.getUsuId()));
                    }
                    rol.setSistema(sistema);
                    if (!rolDto.getUsuarios().isEmpty()) {
                        for (UsuarioDto usuarioDto : rolDto.getUsuarios()) {
                            if (usuarioDto.getModificado()) {
                                Usuario usuario = em.find(Usuario.class, usuarioDto.getUsuId());
                                usuario.getRoles().add(rol);
                                rol.getUsuarios().add(usuario);
                            }
                        }
                    }
                    rol = em.merge(rol);
                } else {
                    rol = new Rol(rolDto);
                    rol.setSistema(sistema);
                    em.persist(rol);
                }
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Rol", new RolDto(rol));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el rol.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el rol.", "guardarRol " + ex.getMessage());
        }
    }

    public Respuesta eliminarRol(Long id) {
        try {
            Rol rol;
            if (id != null && id > 0) {
                rol = em.find(Rol.class, id);
                if (rol == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encrontró el rol a eliminar.", "eliminarRol NoResultException");
                }
                em.remove(rol);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el rol a eliminar.", "eliminarRol NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el rol porque tiene relaciones con otros registros.", "eliminarRol " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al eliminar el rol.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el rol.", "eliminarRol " + ex.getMessage());
        }
    }
}
