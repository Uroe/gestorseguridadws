/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridadws.controller;

import cr.ac.una.gestorseguridadws.model.RolDto;
import cr.ac.una.gestorseguridadws.model.SistemaDto;
import cr.ac.una.gestorseguridadws.model.UsuarioDto;
import cr.ac.una.gestorseguridadws.service.RolService;
import cr.ac.una.gestorseguridadws.service.SistemaService;
import cr.ac.una.gestorseguridadws.service.UsuarioService;
import cr.ac.una.gestorseguridadws.util.CodigoRespuesta;
import cr.ac.una.gestorseguridadws.util.JwTokenHelper;
import cr.ac.una.gestorseguridadws.util.Respuesta;
import cr.ac.una.gestorseguridadws.util.Secure;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author Usuario
 */
@Secure
@WebService(serviceName = "GestorSeguridadWs")
public class GestorSeguridadWs {

    @Context
    SecurityContext securityContext;

//----------------Usuarios----------------//
    @EJB
    UsuarioService usuarioService;

    @WebMethod(operationName = "validarUsuario")
    public UsuarioDto validarUsuario(@WebParam(name = "usuario") String usuario, @WebParam(name = "clave") String clave) {
        Respuesta res = usuarioService.validarUsuario(usuario, clave);
        if (!res.getEstado()) {

        }
        UsuarioDto usuarioDto = (UsuarioDto) res.getResultado("Usuario");
        usuarioDto.setToken(JwTokenHelper.getInstance().generatePrivateKey(usuario));
        return usuarioDto;
    }

    @WebMethod(operationName = "getUsuario")
    public UsuarioDto getUsuario(@WebParam(name = "id") Long id) {
        Respuesta res = usuarioService.getUsuario(id);
        if (!res.getEstado()) {

        }
        UsuarioDto usuarioDto = (UsuarioDto) res.getResultado("Usuario");
        return usuarioDto;
    }

    @WebMethod(operationName = "getUsuarios")
    public List<UsuarioDto> getUsuarios() {
        Respuesta res = usuarioService.getUsuarios();
        if (!res.getEstado()) {

        }
        List<UsuarioDto> usuarios = (List<UsuarioDto>) res.getResultado("Usuarios");
        return usuarios;
    }

    @WebMethod(operationName = "guardarUsuario")
    public UsuarioDto guardarUsuario(@WebParam(name = "usuario") UsuarioDto usuario) {
        Respuesta res = usuarioService.guardarUsuario(usuario);
        if (!res.getEstado()) {

        }
        UsuarioDto usuarioDto = (UsuarioDto) res.getResultado("Usuario");
        return usuarioDto;
    }

    @WebMethod(operationName = "activarUsuario")
    public UsuarioDto activarUsuario(@WebParam(name = "id") Long id) {
        Respuesta res = usuarioService.activarUsuario(id);
        if (!res.getEstado()) {

        }
        UsuarioDto usuarioDto = (UsuarioDto) res.getResultado("Usuario");
        return usuarioDto;
    }

    @WebMethod(operationName = "asignarAdministrador")
    public UsuarioDto asignarAdministrador(@WebParam(name = "id") Long id) {
        Respuesta res = usuarioService.asignarAdministrador(id);
        if (!res.getEstado()) {

        }
        UsuarioDto usuarioDto = (UsuarioDto) res.getResultado("Usuario");
        return usuarioDto;
    }

    @WebMethod(operationName = "cambiarClave")
    public UsuarioDto cambiarClave(@WebParam(name = "claveT") String claveT, @WebParam(name = "claveN") String claveN) {
        Respuesta res = usuarioService.cambiarClave(claveT, claveN);
        if (!res.getEstado()) {

        }
        UsuarioDto usuarioDto = (UsuarioDto) res.getResultado("Usuario");
        return usuarioDto;
    }

    @WebMethod(operationName = "generarClaveTemp")
    public UsuarioDto generarClaveTemp(@WebParam(name = "correo") String correo) {
        Respuesta res = usuarioService.generarClaveTemp(correo);
        if (!res.getEstado()) {

        }
        UsuarioDto usuarioDto = (UsuarioDto) res.getResultado("Usuario");
        return usuarioDto;
    }

    @WebMethod(operationName = "eliminarUsuario")
    public Respuesta eliminarUsuario(@WebParam(name = "id") Long id) {
        Respuesta res = usuarioService.eliminarUsuario(id);
        if (!res.getEstado()) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "", "");
        }
        return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
    }

    //----------------Sistemas----------------//
    @EJB
    SistemaService sistemaService;

    @WebMethod(operationName = "getSistema")
    public SistemaDto getSistema(@WebParam(name = "id") Long id) {
        Respuesta res = sistemaService.getSistema(id);
        if (!res.getEstado()) {

        }
        SistemaDto sistemaDto = (SistemaDto) res.getResultado("Sistema");
        return sistemaDto;
    }

    @WebMethod(operationName = "getSistemas")
    public List<SistemaDto> getSistemas() {
        Respuesta res = sistemaService.getSistemas();
        if (!res.getEstado()) {

        }
        List<SistemaDto> sistemas = (List<SistemaDto>) res.getResultado("Sistemas");
        return sistemas;
    }

    @WebMethod(operationName = "guardarSistema")
    public SistemaDto guardarSistema(@WebParam(name = "sistema") SistemaDto sistema) {
        Respuesta res = sistemaService.guardarSistema(sistema);
        if (!res.getEstado()) {

        }
        SistemaDto sistemaDto = (SistemaDto) res.getResultado("Sistema");
        return sistemaDto;
    }

    @WebMethod(operationName = "eliminarSistema")
    public Respuesta eliminarSistema(@WebParam(name = "id") Long id) {
        Respuesta res = sistemaService.eliminarSistema(id);
        if (!res.getEstado()) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "", "");
        }
        return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
    }

    //----------------Roles----------------//
    @EJB
    RolService rolService;

    @WebMethod(operationName = "getRol")
    public RolDto getRol(@WebParam(name = "id") Long id) {
        Respuesta res = rolService.getRol(id);
        if (!res.getEstado()) {

        }
        RolDto rolDto = (RolDto) res.getResultado("Rol");
        return rolDto;
    }

    @WebMethod(operationName = "getRoles")
    public List<RolDto> getRoles(@WebParam(name = "id") Long id) {
        Respuesta res = rolService.getRoles(id);
        if (!res.getEstado()) {

        }
        List<RolDto> roles = (List<RolDto>) res.getResultado("Roles");
        return roles;
    }

    @WebMethod(operationName = "guardarRol")
    public RolDto guardarRol(@WebParam(name = "rol") RolDto rol) {
        Respuesta res = rolService.guardarRol(rol);
        if (!res.getEstado()) {

        }
        RolDto rolDto = (RolDto) res.getResultado("Rol");
        return rolDto;
    }

    @WebMethod(operationName = "eliminarRol")
    public Respuesta eliminarRol(@WebParam(name = "id") Long id) {
        Respuesta res = rolService.eliminarRol(id);
        if (!res.getEstado()) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "", "");
        }
        return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
    }

}
