/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridadws.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "GESSEG_USUARIOS", schema = "SIGECEUNA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u"),
    @NamedQuery(name = "Usuario.findByUsuId", query = "SELECT u FROM Usuario u WHERE u.id = :id"),
    @NamedQuery(name = "Usuario.findByUsuClave", query = "SELECT u FROM Usuario u WHERE u.usuario = :usuario and u.clave = :clave", hints = @QueryHint(name = "eclipselink.refresh", value = "true")),
    @NamedQuery(name = "Usuario.findByUsuCorreo", query = "SELECT u FROM Usuario u WHERE u.correo = :correo"),
    @NamedQuery(name = "Usuario.findByUsuClaveTemp", query = "SELECT u FROM Usuario u WHERE u.claveTemp = :claveTemp")
})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "GESSEG_USUARIOS_USU_ID_GENERATOR", sequenceName = "SIGECEUNA.GESSEG_USUARIOS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GESSEG_USUARIOS_USU_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "USU_ID")
    private Long id;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "USU_NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "USU_PAPELLIDO")
    private String pApellido;
    @Basic(optional = false)
    @Size(min = 1, max = 20)
    @Column(name = "USU_SAPELLIDO")
    private String sApellido;
    @Basic(optional = false)
    @Size(min = 1, max = 15)
    @Column(name = "USU_CEDULA")
    private String cedula;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "USU_CORREO")
    private String correo;
    @Size(max = 15)
    @Column(name = "USU_TELEFONO")
    private String telefono;
    @Basic(optional = false)
    @Size(min = 1, max = 15)
    @Column(name = "USU_CELULAR")
    private String celular;
    @Basic(optional = false)
    @Size(min = 1, max = 15)
    @Column(name = "USU_USUARIO")
    private String usuario;
    @Basic(optional = false)
    @Size(min = 1, max = 15)
    @Column(name = "USU_CLAVE")
    private String clave;
    @Size(max = 15)
    @Column(name = "USU_CLAVETEM")
    private String claveTemp;
    @Basic(optional = false)
    @Lob
    @Column(name = "USU_FOTO")
    private byte[] foto;
    @Size(max = 1)
    @Column(name = "USU_IDIOMA")
    private String idioma;
    @Size(max = 1)
    @Column(name = "USU_ADMINISTRADOR")
    private String administrador;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "USU_ESTADO")
    private String estado;
    @Version
    @Basic(optional = false)
    @Column(name = "USU_VERSION")
    private Long version;
    @JoinTable(name = "GESSEG_USUARIOSROLES", joinColumns = {
        @JoinColumn(name = "UXR_IDUSU", referencedColumnName = "USU_ID")}, inverseJoinColumns = {
        @JoinColumn(name = "UXR_IDROL", referencedColumnName = "ROL_ID")})
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Rol> roles;

    public Usuario() {
        roles = new ArrayList<>();
    }

    public Usuario(Long usuId) {
        this.id = usuId;
    }

    public Usuario(Long usuId, String usuNombre, String usuPapellido, String usuSapellido, String usuCedula, String usuCorreo, String usuCelular, byte[] usuFoto, String usuEstado, Long usuVersion) {
        this.id = usuId;
        this.nombre = usuNombre;
        this.pApellido = usuPapellido;
        this.sApellido = usuSapellido;
        this.cedula = usuCedula;
        this.correo = usuCorreo;
        this.celular = usuCelular;
        this.foto = usuFoto;
        this.estado = usuEstado;
    }

    public Usuario(UsuarioDto usuario) {
        this.id = usuario.getUsuId();
        actualizarUsuario(usuario);

    }

    public void actualizarUsuario(UsuarioDto usuario) {
        this.nombre = usuario.getUsuNombre();
        this.pApellido = usuario.getUsuPapellido();
        this.sApellido = usuario.getUsuSapellido();
        this.cedula = usuario.getUsuCedula();
        this.correo = usuario.getUsuCorreo();
        this.celular = usuario.getUsuCelular();
        this.usuario = usuario.getUsuUsuario();
        this.clave = usuario.getUsuClave();
        this.foto = usuario.getUsuFoto();
        this.estado = usuario.getUsuEstado();
        this.claveTemp = usuario.getUsuClaveTemp();
        this.idioma = usuario.getUsuIdioma();
        this.administrador = usuario.getUsuAdministrador();
        this.telefono = usuario.getUsuTelefono();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getpApellido() {
        return pApellido;
    }

    public void setpApellido(String pApellido) {
        this.pApellido = pApellido;
    }

    public String getsApellido() {
        return sApellido;
    }

    public void setsApellido(String sApellido) {
        this.sApellido = sApellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getClaveTemp() {
        return claveTemp;
    }

    public void setClaveTemp(String claveTemp) {
        this.claveTemp = claveTemp;
    }

    public Serializable getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getAdministrador() {
        return administrador;
    }

    public void setAdministrador(String administrador) {
        this.administrador = administrador;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

//    @XmlTransient
    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.gestorseguridadws.model.Usuario[ usuId=" + id + " ]";
    }

}
