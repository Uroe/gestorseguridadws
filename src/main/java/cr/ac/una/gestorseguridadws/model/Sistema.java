/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridadws.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "GESSEG_SISTEMAS", schema = "SIGECEUNA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sistema.findAll", query = "SELECT s FROM Sistema s"),
    @NamedQuery(name = "Sistema.findBySisId", query = "SELECT s FROM Sistema s WHERE s.id = :id")})
public class Sistema implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "GESSEG_SISTEMAS_SIS_ID_GENERATOR", sequenceName = "SIGECEUNA.GESSEG_SISTEMAS_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GESSEG_SISTEMAS_SIS_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "SIS_ID")
    private Long id;
    @Basic(optional = false)
    @Size(min = 1, max = 20)
    @Column(name = "SIS_NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "SIS_ESTADO")
    private String estado;
    @Version
    @Basic(optional = false)
    @Column(name = "SIS_VERSION")
    private Long version;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sistema")
    private List<Rol> roles;

    public Sistema() {
    }

    public Sistema(Long sisId) {
        this.id = sisId;
    }

    public Sistema(Long sisId, String sisNombre, String sisEstado, Long sisVersion) {
        this.id = sisId;
        this.nombre = sisNombre;
        this.estado = sisEstado;
        this.version = sisVersion;
    }
    
    public Sistema(SistemaDto sistemaDto){
        this.id = sistemaDto.getSisId();
        actualizarSistema(sistemaDto);
    }
    
    public void actualizarSistema(SistemaDto sistemaDto){
        this.nombre = sistemaDto.getSisNombre();
        this.estado = sistemaDto.getSisEstado();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @XmlTransient
    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sistema)) {
            return false;
        }
        Sistema other = (Sistema) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.gestorseguridadws.model.Sistema[ sisId=" + id + " ]";
    }

}
