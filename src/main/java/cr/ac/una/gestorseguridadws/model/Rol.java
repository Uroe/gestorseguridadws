/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridadws.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "GESSEG_ROLES", schema = "SIGECEUNA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rol.findAll", query = "SELECT r FROM Rol r"),
    @NamedQuery(name = "Rol.findByRolId", query = "SELECT r FROM Rol r WHERE r.id = :id")})
public class Rol implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "GESSEG_ROLES_ROL_ID_GENERATOR", sequenceName = "SIGECEUNA.GESSEG_ROLES_SEQ01", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GESSEG_ROLES_ROL_ID_GENERATOR")
    @Basic(optional = false)
    @Column(name = "ROL_ID")
    private Long id;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "ROL_NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Size(min = 1, max = 1)
    @Column(name = "ROL_ESTADO")
    private String estado;
    @Version
    @Basic(optional = false)
    @Column(name = "ROL_VERSION")
    private Long version;
    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    private List<Usuario> usuarios;
    @JoinColumn(name = "ROL_IDSIS", referencedColumnName = "SIS_ID")
    @ManyToOne(optional = false)
    private Sistema sistema;

    public Rol() {
    }

    public Rol(Long rolId) {
        this.id = rolId;
    }

    public Rol(Long rolId, String rolNombre, String rolEstado, Long rolVersion) {
        this.id = rolId;
        this.nombre = rolNombre;
        this.estado = rolEstado;
        this.version = rolVersion;
    }

    public Rol(RolDto rolDto) {
        this.id = rolDto.getRolId();
        actualizarRol(rolDto);
    }

    public void actualizarRol(RolDto rolDto) {
        this.nombre = rolDto.getRolNombre();
        this.estado = rolDto.getRolEstado();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public Sistema getSistema() {
        return sistema;
    }

    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rol)) {
            return false;
        }
        Rol other = (Rol) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cr.ac.una.gestorseguridadws.model.Rol[ rolId=" + id + " ]";
    }

}
