/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridadws.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Usuario
 */
public class UsuarioDto {

    private Long usuId;
    private String usuNombre;
    private String usuPapellido;
    private String usuSapellido;
    private String usuCedula;
    private String usuCorreo;
    private String usuTelefono;
    private String usuCelular;
    private String usuUsuario;
    private String usuClave;
    private String usuClaveTemp;
    private String usuIdioma;
    private String usuAdministrador;
    private String usuEstado;
    private byte[] usuFoto;
    private Boolean modificado;
    private String token;
    private List<RolDto> roles;

    public UsuarioDto() {
        this.modificado = false;
        roles = new ArrayList<>();
    }

    public UsuarioDto(Usuario usuario) {
        this();
        this.usuId = usuario.getId();
        this.usuNombre = usuario.getNombre();
        this.usuPapellido = usuario.getpApellido();
        this.usuSapellido = usuario.getsApellido();
        this.usuCedula = usuario.getCedula();
        this.usuCorreo = usuario.getCorreo();
        this.usuTelefono = usuario.getTelefono();
        this.usuCelular = usuario.getCelular();
        this.usuUsuario = usuario.getUsuario();
        this.usuClave = usuario.getClave();
        this.usuClaveTemp = usuario.getClaveTemp();
        this.usuIdioma = usuario.getIdioma();
        this.usuAdministrador = usuario.getAdministrador();
        this.usuEstado = usuario.getEstado();
        this.usuFoto = (byte[]) usuario.getFoto();
    }

    public Long getUsuId() {
        return usuId;
    }

    public void setUsuId(Long usuId) {
        this.usuId = usuId;
    }

    public String getUsuNombre() {
        return usuNombre;
    }

    public void setUsuNombre(String usuNombre) {
        this.usuNombre = usuNombre;
    }

    public String getUsuPapellido() {
        return usuPapellido;
    }

    public void setUsuPapellido(String usuPapellido) {
        this.usuPapellido = usuPapellido;
    }

    public String getUsuSapellido() {
        return usuSapellido;
    }

    public void setUsuSapellido(String usuSapellido) {
        this.usuSapellido = usuSapellido;
    }

    public String getUsuCedula() {
        return usuCedula;
    }

    public void setUsuCedula(String usuCedula) {
        this.usuCedula = usuCedula;
    }

    public String getUsuCorreo() {
        return usuCorreo;
    }

    public void setUsuCorreo(String usuCorreo) {
        this.usuCorreo = usuCorreo;
    }

    public String getUsuTelefono() {
        return usuTelefono;
    }

    public void setUsuTelefono(String usuTelefono) {
        this.usuTelefono = usuTelefono;
    }

    public String getUsuCelular() {
        return usuCelular;
    }

    public void setUsuCelular(String usuCelular) {
        this.usuCelular = usuCelular;
    }

    public String getUsuUsuario() {
        return usuUsuario;
    }

    public void setUsuUsuario(String usuUsuario) {
        this.usuUsuario = usuUsuario;
    }

    public String getUsuClave() {
        return usuClave;
    }

    public void setUsuClave(String usuClave) {
        this.usuClave = usuClave;
    }

    public String getUsuClaveTemp() {
        return usuClaveTemp;
    }

    public void setUsuClaveTemp(String usuClaveTemp) {
        this.usuClaveTemp = usuClaveTemp;
    }

    public String getUsuIdioma() {
        return usuIdioma;
    }

    public void setUsuIdioma(String usuIdioma) {
        this.usuIdioma = usuIdioma;
    }

    public String getUsuAdministrador() {
        return usuAdministrador;
    }

    public void setUsuAdministrador(String usuAdministrador) {
        this.usuAdministrador = usuAdministrador;
    }

    public String getUsuEstado() {
        return usuEstado;
    }

    public void setUsuEstado(String usuEstado) {
        this.usuEstado = usuEstado;
    }

    public byte[] getUsuFoto() {
        return usuFoto;
    }

    public void setUsuFoto(byte[] usuFoto) {
        this.usuFoto = usuFoto;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<RolDto> getRoles() {
        return roles;
    }

    public void setRoles(List<RolDto> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "UsuarioDto{" + "usuId=" + usuId + ", usuNombre=" + usuNombre + ", usuPapellido=" + usuPapellido + ", usuSapellido=" + usuSapellido + ", usuCedula=" + usuCedula + ", usuCorreo=" + usuCorreo + ", usuTelefono=" + usuTelefono + ", usuCelular=" + usuCelular + ", usuUsuario=" + usuUsuario + ", usuClave=" + usuClave + ", usuClaveTemp=" + usuClaveTemp + ", usuIdioma=" + usuIdioma + ", usuAdministrador=" + usuAdministrador + ", usuEstado=" + usuEstado + ", usuFoto=" + usuFoto + ", modificado=" + modificado + ", token=" + token + '}';
    }

}
