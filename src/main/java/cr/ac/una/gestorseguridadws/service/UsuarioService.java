/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridadws.service;

import cr.ac.una.gestorseguridadws.model.Rol;
import cr.ac.una.gestorseguridadws.model.RolDto;
import cr.ac.una.gestorseguridadws.model.Usuario;
import cr.ac.una.gestorseguridadws.model.UsuarioDto;
import cr.ac.una.gestorseguridadws.util.CodigoRespuesta;
import cr.ac.una.gestorseguridadws.util.Respuesta;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Usuario
 */
@Stateless
@LocalBean
public class UsuarioService {

    private static final Logger LOG = Logger.getLogger(UsuarioService.class.getName());

    @PersistenceContext(unitName = "GestorSeguridadWsPU")
    private EntityManager em;

    public Respuesta validarUsuario(String usuario, String clave) {
        try {
            Query qryActividad = em.createNamedQuery("Usuario.findByUsuClave", Usuario.class);
            qryActividad.setParameter("usuario", usuario);
            qryActividad.setParameter("clave", clave);

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new UsuarioDto((Usuario) qryActividad.getSingleResult()));

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un usuario con las credenciales ingresadas.", "validarUsuario NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "validarUsuario NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "validarUsuario " + ex.getMessage());
        }
    }

    public Respuesta getUsuario(Long id) {
        try {
            Query qryUsuario = em.createNamedQuery("Usuario.findByUsuId", Usuario.class);
            qryUsuario.setParameter("id", id);

            Usuario usuario = (Usuario) qryUsuario.getSingleResult();
            UsuarioDto usuarioDto = new UsuarioDto(usuario);

            for (Rol role : usuario.getRoles()) {
                usuarioDto.getRoles().add(new RolDto(role));
            }

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", usuarioDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No existe un usuario con el código ingresado.", "getUsuario NoResultException");
        } catch (NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "getUsuario NonUniqueResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar el empleado.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar el usuario.", "getUsuario " + ex.getMessage());
        }
    }

    public Respuesta getUsuarios() {
        try {
            Query qryUsuario = em.createNamedQuery("Usuario.findAll", Usuario.class);
            List<Usuario> usuarios = qryUsuario.getResultList();

            List<UsuarioDto> usuariosDto = new ArrayList<>();

            usuarios.forEach(usuario -> {
                usuariosDto.add(new UsuarioDto(usuario));
            });

            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuarios", usuariosDto);

        } catch (NoResultException ex) {
            return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Ocurrio un error al consultar los usuarios.", "getUsuarios NoResultException");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al consultar los usuarios.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al consultar los usuarios.", "getUsuarios " + ex.getMessage());
        }
    }

    public Respuesta guardarUsuario(UsuarioDto usuarioDto) {
        try {
            Usuario usuario;
            if (usuarioDto.getUsuId() != null && usuarioDto.getUsuId() > 0) {
                usuario = em.find(Usuario.class, usuarioDto.getUsuId());
                if (usuario == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el usuario a modificar.", "guardarUsuario NoResultException");
                }
                usuario.actualizarUsuario(usuarioDto);
                usuario = em.merge(usuario);
            } else {
                usuario = new Usuario(usuarioDto);
                em.persist(usuario);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new UsuarioDto(usuario));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar el usuario.", "guardarUsuario " + ex.getMessage());
        }
    }

    public Respuesta activarUsuario(Long id) {
        try {
            Usuario usuario = new Usuario();
            if (id != null && id > 0) {
                usuario = em.find(Usuario.class, id);
                if (usuario == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el usuario a modificar.", "guardarUsuario NoResultException");
                }
                usuario.setEstado("A");
                usuario = em.merge(usuario);
            } else {
                em.persist(usuario);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new UsuarioDto(usuario));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al activar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al activar el usuario.", "activarUsuario " + ex.getMessage());
        }
    }

    public Respuesta cambiarClave(String claveT, String claveN) {
        try {
            Query qryUsuario = em.createNamedQuery("Usuario.findByUsuClaveTemp", Usuario.class);
            qryUsuario.setParameter("claveTemp", claveT);

            Usuario usuario = (Usuario) qryUsuario.getSingleResult();

            if (usuario == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el usuario a modificar.", "generarClaveTemp NoResultException");
            } else {
                usuario.setClave(claveN);
                usuario = em.merge(usuario);
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new UsuarioDto(usuario));
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al cambiar la clave.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al cambiar la clave.", "cambiarClave " + ex.getMessage());
        }
    }

    public Respuesta generarClaveTemp(String correo) {
        try {
            Query qryUsuario = em.createNamedQuery("Usuario.findByUsuCorreo", Usuario.class);
            qryUsuario.setParameter("correo", correo);

            Usuario usuario = (Usuario) qryUsuario.getSingleResult();

            if (usuario == null) {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el usuario a modificar.", "generarClaveTemp NoResultException");
            } else {
                Random r = new Random(); // Intialize a Random Number Generator with SysTime as the seed
                StringBuilder sb = new StringBuilder(10);
                for (int i = 0; i < 10; i++) { // For each letter in the word
                    char tmp = (char) ('a' + r.nextInt('z' - 'a')); // Generate a letter between a and z
                    sb.append(tmp); // Add it to the String
                }
                usuario.setClaveTemp(sb.toString());
                usuario = em.merge(usuario);
                em.flush();
                return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new UsuarioDto(usuario));
            }

        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al generar la clave.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al generar la clave.", "generarClaveTemp " + ex.getMessage());
        }
    }

    public Respuesta asignarAdministrador(Long id) {
        try {
            Usuario usuario = new Usuario();
            if (id != null && id > 0) {
                usuario = em.find(Usuario.class, id);
                if (usuario == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el usuario a modificar.", "asignarAdministrador NoResultException");
                }
                usuario.setAdministrador("S");
                usuario = em.merge(usuario);
            } else {
                em.persist(usuario);
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "", "Usuario", new UsuarioDto(usuario));
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al guardar.", "asignarAdministrador " + ex.getMessage());
        }
    }

    public Respuesta eliminarUsuario(Long id) {
        try {
            Usuario usuario;
            if (id != null && id > 0) {
                usuario = em.find(Usuario.class, id);
                if (usuario == null) {
                    return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "No se encontró el usuario a eliminar.", "eliminarUsuario NoResultException");
                }
                em.remove(usuario);
            } else {
                return new Respuesta(false, CodigoRespuesta.ERROR_NOENCONTRADO, "Debe cargar el usuario a eliminar.", "eliminarUsuario NoResultException");
            }
            em.flush();
            return new Respuesta(true, CodigoRespuesta.CORRECTO, "", "");
        } catch (Exception ex) {
            if (ex.getCause() != null && ex.getCause().getCause().getClass() == SQLIntegrityConstraintViolationException.class) {
                return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "No se puede eliminar el usuario porque tiene relaciones con otros registros.", "eliminarUsuario " + ex.getMessage());
            }
            LOG.log(Level.SEVERE, "Ocurrio un error al guardar el usuario.", ex);
            return new Respuesta(false, CodigoRespuesta.ERROR_INTERNO, "Ocurrio un error al eliminar el usuario.", "eliminarUsuario " + ex.getMessage());
        }
    }

}
