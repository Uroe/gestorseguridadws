/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.gestorseguridadws.model;

/**
 *
 * @author JosueNG
 */
public class SistemaDto {

    private Long sisId;
    private String sisNombre;
    private String sisEstado;
    private Boolean modificado;

    public SistemaDto() {
        this.modificado = false;
    }

    public SistemaDto(Sistema sistema) {
        this();
        this.sisId = sistema.getId();
        this.sisNombre = sistema.getNombre();
        this.sisEstado = sistema.getEstado();
    }

    public Long getSisId() {
        return sisId;
    }

    public void setSisId(Long sisId) {
        this.sisId = sisId;
    }

    public String getSisNombre() {
        return sisNombre;
    }

    public void setSisNombre(String sisNombre) {
        this.sisNombre = sisNombre;
    }

    public String getSisEstado() {
        return sisEstado;
    }

    public void setSisEstado(String sisEstado) {
        this.sisEstado = sisEstado;
    }

    public Boolean getModificado() {
        return modificado;
    }

    public void setModificado(Boolean modificado) {
        this.modificado = modificado;
    }

    @Override
    public String toString() {
        return "SistemaDto{" + "id=" + sisId + ", nombre=" + sisNombre + ", estado=" + sisEstado + '}';
    }

}
